# Prerequisites

* PHP >= 7.3
* [Composer](https://getcomposer.org)
* [Symfony CLI](https://symfony.com/download)

## Installation Symfony / EasyAdmin

1. https://github.com/symfony/skeleton - https://symfony.com/doc/current/setup.html `composer create-project symfony/skeleton sided`
2. Install Symfony Maker Bundle (Scaffolding) `composer req --dev maker-bundle`
3. Install EasyAdmin Bundle `composer req admin`
4. Configure Database in `.env` - Use SQLite in the beginning
5. Create the Database `./bin/console doctrine:database:create`
6. Create the Schema `./bin/console doctrine:schema:create`
7. Create an Entity 'Book' `./bin/console make:entity Book` and add `title` and `isbn` as properties
8. Create a Script to Update the database `./bin/console make:migration`
9. Install the migrations bundle `composer req migrations` and re-run `./bin/console make:migration"
10. Run the migration with `./bin/console doctrine:migrations:migrate`
11. Create an Entity 'Author' with properties 'firstName' and 'lastName' `./bin/console make:entity Author`, create a Migration and apply the Migration

12. Start the application with `symfony serve` and head over to https://localhost:8000/admin
13. Configure the Entities to be managed in 'config/packages/easy_admin.yaml'

14. Create a Relation from Book to Author with `./bin/console make:entity Book` and add 'author' as property. Create a migration and apply it

15. Add `__toString` methods to src/Entity/Author and src/Entity/Book (Attention - hardcore Programming)

## Add Rest-API

1. `composer req api` followed by (optonal) `composre req webonyx/graphql-php`
2. Configure Entities as [API-Resources](https://api-platform.com/docs/core/getting-started/)
3. Locate the Browser to https://localhost:8000/api and toy around a bit